# Jojo's Profile
PPW Odd 2020/2021 - Story 1

## Overview
Jojo's Profile contains some information about himself, including his hobbies, educational background, a little description, and also social media. This website shows his progress in learning basic HTML/CSS. This is also his first time using Gitlab CI/CD to deploy a web app to Heroku server. Django framework is used since Heroku cannot host a mere static website.

## Link
Click <http://jojostory1.herokuapp.com> to get to the website.

## Resources
- HTML
- CSS
- Django
- Heroku